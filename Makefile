up:
	podman-compose up -d
status:
	podman-compose ps
down:
	podman-compose down
destroy:
	podman-compose down -v
